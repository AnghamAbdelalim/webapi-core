﻿using Ecommerce.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ecommerce.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[EnableCors("MyAllowSpecificOrigins")]
     //[EnableCors("AllowOrigin")]
    public class ProductController : ControllerBase
    {
       private readonly ApplicationDB _context;
        public bool found = false;
        public ProductController(ApplicationDB context)
        {
            _context = context;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Products>>> Getproducts()
        {
            return await _context.Products.ToListAsync();
        }
       
       
       [Route("getbyId")]
        public async Task<IActionResult> BuyProduct(int id)
        {
              var product = await _context.Products.FindAsync(id);
               product.Quantity--;
               if (product.Quantity > 0)
                {
                    await _context.SaveChangesAsync();
                }
            return Ok(); 
        }
    }
}

