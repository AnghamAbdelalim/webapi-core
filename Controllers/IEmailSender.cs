﻿using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.Messaging;
using System.Threading.Tasks;

namespace Ecommerce.Controllers
{
    internal interface IEmailSender
    {
        Task SendEmailAsync(Message message);
    }
}