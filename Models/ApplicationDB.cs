﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ecommerce.Models
{
    public class ApplicationDB:DbContext
    {
        public ApplicationDB(DbContextOptions<ApplicationDB>options):base(options)
        {

        }
        public virtual DbSet<Products> Products { get; set; }
        public virtual DbSet<User> Users { get; set; }
    }
}
